package io.swagger.tm_client.listener.task;

import io.swagger.client.ApiException;
import io.swagger.client.api.DefaultApi;
import io.swagger.client.model.ProjectDTO;
import io.swagger.client.model.TaskDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import io.swagger.tm_client.event.ConsoleEvent;
import io.swagger.tm_client.listener.AbstractListener;
import io.swagger.tm_client.util.TerminalUtil;

@Component
public final class TaskCreateListener extends AbstractListener {

    @NotNull
    @Autowired
    private DefaultApi api;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-create";
    }

    @NotNull
    @Override
    public String description() {
        return "Create new task.";
    }

    @EventListener(condition = "@taskCreateListener.name() == #event.name")
    public void handler(final ConsoleEvent event) throws ApiException {
        System.out.println("[CREATE TASKS]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK NAME:");
        @NotNull final String taskName = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final ProjectDTO project = api.findById(projectId);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        @NotNull final TaskDTO task = new TaskDTO();
        task.setProjectId(projectId);
        task.setName(taskName);
        task.setDescription(description);
        api.create_0(task);
        System.out.println("[OK]");
    }

}
