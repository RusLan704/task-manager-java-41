<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../include/_header.jsp"/>

   <h1>TASK CREATE</h1>

        <form:form method="POST" action="/tasks/create" modelAttribute="task">
            <p>
                <div style="margin-bottom: 5px;">Name:</div>
                <form:input path="name"/>
            </p>

            <p>
                <div style="margin-bottom: 5px;">Description:</div>
                <form:input path="description"/>
            </p>

            <p>
                <div style="margin-bottom: 5px;">PROJECT:</div>
                     <div>
                         <form:select path="projectId" >
                             <form:option value="${null}" label="--- // ---" />
                             <form:options items="${projects}" itemLabel="name" itemValue="id" />
                         </form:select>
                     </div>
                </div>
            </p>

            <input type="submit" value="CREATE" class="customButton">
        </form:form>

<jsp:include page="../include/_footer.jsp"/>
