package ru.bakhtiyarov.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.exception.AbstractException;

public class EmptyLoginException extends AbstractException {

    @NotNull
    public EmptyLoginException() {
        super("Error! Login is empty...");
    }

}