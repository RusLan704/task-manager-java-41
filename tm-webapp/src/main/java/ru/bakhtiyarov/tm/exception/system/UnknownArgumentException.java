package ru.bakhtiyarov.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.exception.AbstractException;

public class UnknownArgumentException extends AbstractException {

    @NotNull
    public UnknownArgumentException(@NotNull final String arg) {
        super("Error! Command ``" + arg + "`` doesn't exist...");
    }

}