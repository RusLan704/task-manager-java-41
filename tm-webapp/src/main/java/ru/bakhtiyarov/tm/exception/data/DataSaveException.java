package ru.bakhtiyarov.tm.exception.data;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.exception.AbstractException;

public class DataSaveException extends AbstractException {

    @NotNull
    public DataSaveException() {
        super("An error occurred while saving data");
    }

    @NotNull
    public DataSaveException(Throwable cause) {
        super(cause);
    }

}
