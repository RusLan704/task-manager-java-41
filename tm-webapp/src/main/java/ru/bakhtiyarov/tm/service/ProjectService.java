package ru.bakhtiyarov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.bakhtiyarov.tm.api.service.IProjectService;
import ru.bakhtiyarov.tm.api.service.IUserService;
import ru.bakhtiyarov.tm.entity.Project;
import ru.bakhtiyarov.tm.exception.empty.EmptyDescriptionException;
import ru.bakhtiyarov.tm.exception.empty.EmptyIdException;
import ru.bakhtiyarov.tm.exception.empty.EmptyNameException;
import ru.bakhtiyarov.tm.exception.empty.EmptyUserIdException;
import ru.bakhtiyarov.tm.exception.invalid.InvalidProjectException;
import ru.bakhtiyarov.tm.exception.invalid.InvalidTaskException;
import ru.bakhtiyarov.tm.exception.system.IncorrectIndexException;
import ru.bakhtiyarov.tm.repository.IProjectRepository;

import java.util.List;

@Service
public class ProjectService extends AbstractService<Project, IProjectRepository> implements IProjectService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    @Autowired
    public ProjectService(
            @NotNull final IUserService userService,
            @NotNull final IProjectRepository projectRepository
    ) {
        this.userService = userService;
        this.projectRepository = projectRepository;
    }

    @Override
    @SneakyThrows
    @Transactional
    public Project create(@Nullable final String userId, @Nullable final Project project) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) throw new InvalidProjectException();
        if (project.getName() == null || project.getName().isEmpty()) throw new EmptyNameException();
        if (project.getDescription() == null || project.getDescription().isEmpty()) throw new EmptyNameException();
        project.setUser(userService.findById(userId));
        return projectRepository.save(project);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Project updateProjectById(
            @Nullable final String userId,
            @Nullable final Project project
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) return null;
        @Nullable final String id = project.getId();
        @Nullable final String name = project.getName();
        @Nullable final String description = project.getDescription();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        if (description == null) throw new EmptyDescriptionException();
        @Nullable final Project projectUpdated = findOneById(userId, id);
        if (projectUpdated == null) throw new InvalidTaskException();
        projectUpdated.setId(id);
        projectUpdated.setName(name);
        projectUpdated.setDescription(description);
        projectUpdated.setStatus(project.getStatus());
        projectUpdated.setStartDate(project.getStartDate());
        projectUpdated.setFinishDate(project.getFinishDate());
        return save(projectUpdated);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findOneByUserIdAndId(userId, id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        return projectRepository.findAllByUserId(userId).get(index);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findByUserIdAndName(userId, name);
    }

    @Nullable
    @Override
    public Project findById(@Nullable final String id) {
        if (id == null) throw new EmptyUserIdException();
        return projectRepository.findById(id).get();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return projectRepository.findAllByUserId(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeAll(@Nullable final String userId) {
        if (userId == null) throw new EmptyUserIdException();
        projectRepository.deleteAllByUserId(userId);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @Nullable final Project project = projectRepository.findAllByUserId(userId).get(index);
        projectRepository.deleteById(project.getId());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        projectRepository.deleteById(id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        projectRepository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeOneByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        projectRepository.deleteByUserIdAndName(userId, name);
    }

    @Override
    @Transactional
    public void removeAll() {
        projectRepository.deleteAll();
    }

    @NotNull
    @Override
    public IProjectRepository getRepository() {
        return projectRepository;
    }

}