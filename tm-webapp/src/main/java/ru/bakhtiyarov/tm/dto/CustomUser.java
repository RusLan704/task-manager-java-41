package ru.bakhtiyarov.tm.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

public class CustomUser extends User {

    private String userId;

    public CustomUser withUserId(@NotNull final String userId){
        this.userId = userId;
        return this;
    }

    @NotNull
    public CustomUser(@NotNull final UserDetails user) {
        super(
                user.getUsername(),
                user.getPassword(),
                user.isEnabled(),
                user.isAccountNonExpired(),
                user.isCredentialsNonExpired(),
                user.isAccountNonLocked(),
                user.getAuthorities()
        );
    }

    @NotNull
    public CustomUser(
            @NotNull final String username,
            @NotNull final String password,
            @NotNull final Collection<? extends GrantedAuthority> authorities
    ) {
        super(username, password, authorities);
    }

    @NotNull
    public CustomUser(
            @NotNull final String username,
            @NotNull final String password,
            final boolean enabled,
            final boolean accountNonExpired,
            final boolean credentialsNonExpired,
            final boolean accountNonLocked,
            @NotNull final Collection<? extends GrantedAuthority> authorities
    ) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
