package ru.bakhtiyarov.tm.endpoint.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.bakhtiyarov.tm.api.service.IProjectService;
import ru.bakhtiyarov.tm.api.service.converter.IProjectConverter;
import ru.bakhtiyarov.tm.dto.ProjectDTO;
import ru.bakhtiyarov.tm.entity.Project;
import ru.bakhtiyarov.tm.util.UserUtil;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/rest/project")
public class ProjectRestController {

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final IProjectConverter projectConverter;

    @NotNull
    @Autowired
    public ProjectRestController(
            @NotNull final IProjectService projectService,
            @NotNull IProjectConverter projectConverter
    ) {
        this.projectService = projectService;
        this.projectConverter = projectConverter;
    }

    @Nullable
    @PostMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
    public ProjectDTO create(
            @RequestBody @Nullable final ProjectDTO projectDTO
    ) {
        final Project project = projectService.create(UserUtil.getAuthUserId(), projectConverter.toEntity(projectDTO));
        return projectConverter.toDTO(project);
    }

    @NotNull
    @GetMapping(value = "/findAll", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ProjectDTO> findAll() {
        List<Project> projects = projectService.findAll(UserUtil.getAuthUserId());
        return projects
                .stream()
                .map(projectConverter::toDTO)
                .collect(Collectors.toList());
    }

    @GetMapping(value = "/findById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ProjectDTO findById(
            @PathVariable("id") @Nullable final String id
    ) {
        return projectConverter.toDTO(projectService.findOneById(UserUtil.getAuthUserId(), id));
    }

    @PutMapping(value = "/updateById", produces = MediaType.APPLICATION_JSON_VALUE)
    public ProjectDTO updateProjectById(
            @RequestBody @Nullable final ProjectDTO projectDTO
    ) {
        @Nullable Project project = projectService.updateProjectById(
                UserUtil.getAuthUserId(),
                projectConverter.toEntity(projectDTO)
        );
        return projectConverter.toDTO(project);
    }

    @DeleteMapping(value = "/removeById/{id}")
    public void removeOneById(
            @PathVariable("id") @Nullable final String id
    ) {
        projectService.removeOneById(UserUtil.getAuthUserId(), id);
    }

}